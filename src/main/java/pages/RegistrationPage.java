package pages;

import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class RegistrationPage {

    public RegistrationPage setUsername(String username){
        $(By.name("username")).setValue(username);
        return this;
    }

    public RegistrationPage setPassword(String password){
        $(By.name("password")).setValue(password);
        return this;
    }

    public RegistrationPage confirmPassword(String confirmPassword){
        $(By.name("passwordConfirm")).setValue(confirmPassword);
        return this;
    }

    public RegistrationPage setFirstname(String firstname){
        $(By.name("firstName")).setValue(firstname);
        return this;
    }

    public RegistrationPage setLastname(String lastname){
        $(By.name("lastName")).setValue(lastname);
        return this;
    }

    public RegistrationPage setStreet(String street){
        $(By.name("address1")).setValue(street);
        return this;
    }

    public RegistrationPage setCity(String city){
        $(By.name("address2")).setValue(city);
        return this;
    }

    public void confirmRegistration(){
        $(By.name("register")).click();
    }

    public RegistrationPage openRegistrationPage(){
        open("http://localhost:1080/cgi-bin/login.pl?username=&password=&getInfo=true");
        return this;
    }
}
