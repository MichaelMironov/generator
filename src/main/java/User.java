public class User {
    private String username;
    private String password;
    private String passwordConfirm;
    private String creditCard;
    private String seatPref;
    private String seatType;
    private String expDate;
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;

    public User(String username, String password, String passwordConfirm, String creditCard, String seatPref, String seatType, String expDate, String firstName, String lastName, String address1, String address2) {
        this.username = username;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
        this.creditCard = creditCard;
        this.seatPref = seatPref;
        this.seatType = seatType;
        this.expDate = expDate;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address1 = address1;
        this.address2 = address2;
    }
    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @Override
    public String toString() {
        return username + ',' + password + ',' + creditCard + ',' + seatPref + ',' + seatType + ',' + expDate + ',' + firstName + ',' + lastName + ',' + address1 + ',' +
                address2 + '\n';
    }
}
