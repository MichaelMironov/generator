import com.codeborne.selenide.Configuration;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang3.RandomStringUtils;
import pages.RegistrationPage;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
public class Generator {

    public static void main(String[] args)  {
        Configuration.browser = "firefox";
        Configuration.headless = true;
        WebDriverManager.firefoxdriver().setup();

        String pathDatePool = "E:\\VuGen\\Scripts\\lessons\\mironov\\Users.dat";
        int userCount = 20;

        try(FileWriter writer = new FileWriter(pathDatePool, true)){

            String headers = "USERNAME,PASSWORD,CREDITCARD,SEATPREF,SEATYPE,EXPDATE,FIRSTNAME,LASTNAME,ADDRESS1,ADDRESS2\n";
            writer.write(headers);

            for (int i = 0; i < userCount; i++) {

                String pass = generateNumbers();

                //Create a new user with random data
                User user = new User(generateStrings(),pass,pass,generateNumbers(),randomSetPref(),randomSetType(),randomExpDate(),generateStrings(),generateStrings(),generateStrings(),generateStrings());

                registerNewUser(user); /*Registering a generated user*/

                writer.write(user.toString()); /*Writing to file registered user*/

                writer.flush();
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    static String generateStrings(){
        return RandomStringUtils.randomAlphabetic(5);
    }

    static String generateNumbers(){
        Random rnd = new Random();
        return String.valueOf(rnd.nextInt(1234567890));
    }

    static String randomSetPref(){
        String[] setPref = new String[]{"Aisle", "Window", "None"};
        int n = (int)Math.floor(Math.random() * setPref.length);
        return setPref[n];
    }

    static String randomSetType(){
        String[] setType = new String[]{"First", "Business", "Coach"};
        int n = (int)Math.floor(Math.random() * setType.length);
        return setType[n];
    }

    static String randomExpDate(){
        int randomMonth = 1 + (int) (Math.random() * 12);
        int randomYear = 23 + (int) (Math.random() * 10);
        return randomMonth + "/" + randomYear;
    }

    static void registerNewUser(User user){

        RegistrationPage registrationPage = new RegistrationPage();

        registrationPage
                .openRegistrationPage()
                .setUsername(user.getUsername())
                .setPassword(user.getPassword())
                .confirmPassword(user.getPasswordConfirm())
                .setFirstname(user.getFirstName())
                .setLastname(user.getLastName())
                .setStreet(user.getAddress1())
                .setCity(user.getAddress2())
                .confirmRegistration();
    }
}